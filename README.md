## Purpose

This console application allows the user to modify his Smugmug keywords for all images
in his galleries.  For example, say that you've been tagging photos for years, in
many galleries, with the keyword "kiteboarding", but you now want to use the keyword
"kitesurfing" instead.  As of October 2016, using [Smugmug's website](https://www.smugmug.com/keyword)
to change these keywords would take days of clicking and typing.

This application automates the process of changing Smugmug keywords.  It will find all
your photos with a particular keyword (ex: "kiteboarding") and change each of them to 
use a new keyword (ex: "kitesurfing") instead.

It's also possible to delete a keyword, or to replace a single keyword with multiple keywords.

## Setup

1. This script runs on [Python 3](https://www.python.org/) (I've tested it with Python 3.5.2).
  If you don't have Python3 installed on your computer, you'll need to [install it](https://www.python.org/downloads/).
  
2. Install the requirements
<p/>
```
pip install -r requirements.txt
```

3. To run the app, you need to have a Smugmug API key. 
[Get your own Smugmug API key](https://api.smugmug.com/api/developer/apply) and save it
in the file [config.json](config.json).

4. Run the script using the command line:
<p/>
```
python3 smugmug-keyword-fixer.py
```

## Usage

The first time you run the app, you'll need to go through the login process:
<p/>

    Looks like you haven't logged in before. So you'll need to go through the OAuth process
    In a web browser, go to:
     https://secure.smugmug.com/services/oauth/1.0a/authorize
    Enter the six-digit code: 

Once you've authorized the app, you'll be greeted with the keyword prompt:

    What keyword would you like to change? (Enter for list, space to stop) >>  

Pick a keyword and the app will show you all the images with that keyword.  This is the
"reality check".  Are you sure that you want to change all of these images?

    Keyword "kiteboarding" is used in these 10 images
    https://wickedsurf.smugmug.com/Surfing/2013-06-22-Sherman-Island/i-MgFZdJS
    https://wickedsurf.smugmug.com/Surfing/2015-05-23-Pismo-Beach/i-wBnPbcN
    https://wickedsurf.smugmug.com/Surfing/2015-01-17-La-Ventana/i-GpmFCpP
    https://wickedsurf.smugmug.com/Surfing/2015-01-17-La-Ventana/i-TZGbk7v
    https://wickedsurf.smugmug.com/Surfing/2015-01-17-La-Ventana/i-jxQmQ2j
    https://wickedsurf.smugmug.com/Surfing/20140710-Kauai-Kiteboarding/i-g4TKhdb
    https://wickedsurf.smugmug.com/Surfing/20140518-Stinson-Kitesurfing/i-PCGhsCq
    https://wickedsurf.smugmug.com/Surfing/2013-07-20-Kiteboarding-with/i-5sv3QZb
    https://wickedsurf.smugmug.com/Surfing/2013-07-20-Kiteboarding-with/i-Vrc6vnF
    https://wickedsurf.smugmug.com/Surfing/2015-05-23-Pismo-Beach/i-Cz9H5g4


You'll be prompted to enter the replacement keywords (e.g., the ones that will 
replace the keyword "kiteboarding"):

    Replacement keywords (separated by semicolons, "DELETE" to delete) >> 


At this prompt, you have 4 main options:

1. Type in a new keyword (ex: kitesurfing) and hit enter.  The app will proceed to update
each of the images to use the new keyword.

2. Type in two or more new keywords, separated by semicolons (Ex: "kite; surfing" where "kite" is one keyword and "surfing" is another keyword).  The app will replace the original keyword with all of the new keywords.

3. Instead of a keyword, enter the word "DELETE" (no quotes).  The app will delete the original
keyword from all your photos and replace it with nothing.  If you want to tag photos
with with keyword "delete", then you're out of luck; this app can't help you (or you can
modify the script yourself).

4. Leave the field blank and hit *Enter*.  The app will assume that you don't want to change
the keyword after all and do nothing.

Once the action is complete, the app will take you back to the "What keyword do you want to change?" prompt.

## Other Notes

This app uses [Smugmug API v2](https://api.smugmug.com/api/v2/doc).  See the 
[Smugmug API Wiki](https://smugmug.atlassian.net/wiki/display/API/Home) for more information.