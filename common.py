'''
This code was copied from the Smugmug OAuth Example Code, which you can download
in its entirety from:
  https://gist.github.com/smugkarl/10046914

Additional documentation of the Smugmug v2 API is available at:
  https://api.smugmug.com/api/v2/doc/
'''

import json
from rauth import OAuth1Service, OAuth1Session
import sys
from urllib.parse import urlsplit, urlunsplit, parse_qsl, urlencode

OAUTH_ORIGIN = 'https://secure.smugmug.com'
REQUEST_TOKEN_URL = OAUTH_ORIGIN + '/services/oauth/1.0a/getRequestToken'
ACCESS_TOKEN_URL = OAUTH_ORIGIN + '/services/oauth/1.0a/getAccessToken'
AUTHORIZE_URL = OAUTH_ORIGIN + '/services/oauth/1.0a/authorize'

API_ORIGIN = 'https://api.smugmug.com'

SERVICE = None  # OAuth Service
SESSION = None  # OAuth Session (after access_token granted)

def get_service():
    '''
    Read the API Key from the config.json file and create an OAuth
    Service that can be used to access the Smugmug API on behalf of
    a particular user.
    '''
    global SERVICE
    if SERVICE is None:
        try:
            with open('config.json', 'r') as fh:
                config = json.load(fh)
        except IOError as e:
            print('====================================================')
            print('Failed to open config.json! Did you create it?')
            print('The expected format is demonstrated in example.json.')
            print('====================================================')
            sys.exit(1)
        if type(config) is not dict \
                or 'key' not in config \
                or 'secret' not in config\
                or type(config['key']) is not str \
                or type(config['secret']) is not str:
            print('====================================================')
            print('Invalid config.json!')
            print('The expected format is demonstrated in example.json.')
            print('====================================================')
            sys.exit(1)
        if config['key'] == "<YOUR-SMUGMUG-API-KEY-GOES-HERE>":
            print('You need to get a Smugmug API key from:\n',
                  'https://api.smugmug.com/api/developer/apply',
                  '\nThen enter it into config.json')
            sys.exit(1)

        # If we got this far, then we should be able to generate an OAuthService
        SERVICE = OAuth1Service(
                name='smugmug-oauth-web-demo',
                consumer_key=config['key'],
                consumer_secret=config['secret'],
                request_token_url=REQUEST_TOKEN_URL,
                access_token_url=ACCESS_TOKEN_URL,
                authorize_url=AUTHORIZE_URL,
                base_url=API_ORIGIN + '/api/v2')
    return SERVICE


def get_session():
    '''
    Check if we're already logged into the Smugmug service for a particular user.
    If so, then continue using the existing access_token.
    '''
    global SESSION
    if SESSION is None:
        service = get_service()
        (access_token, access_token_secret) = load_access_token()
        if access_token and access_token_secret:
            SESSION = OAuth1Session(service.consumer_key,
                                    service.consumer_secret,
                                    access_token=access_token,
                                    access_token_secret=access_token_secret)
    return SESSION


def load_access_token():
    try:
        with open('config.json', 'r') as fh:
            config = json.load(fh)
    except IOError as e:
        print('Failed to open config.json')
        return (None, None)
    if type(config) is not dict \
            or 'access_token' not in config \
            or 'access_token_secret' not in config \
            or type(config['access_token']) is not str \
            or type(config['access_token_secret']) is not str \
            or not config['access_token'] \
            or not config['access_token_secret']:
        # No access token means the user is not logged in yet
        return (None, None)
    else:
        return(config['access_token'], config['access_token_secret'])

def save_access_token(access_token, access_token_secret):
    '''
    Save the access_token and secret to config.json
    '''
    try:
        with open('config.json', 'r') as fh:
            config = json.load(fh)
    except IOError as e:
        print('Failed to open config.json')
        sys.exit(1)
    if type(config) is not dict:
        print('Warning: config.json was not formatted correctly')
        config = {}
    # Update config to include the new access_token and secret
    config['access_token'] = access_token
    config['access_token_secret'] = access_token_secret
    # Rewrite config.json
    try:
        with open('config.json', 'w') as fh:
            config = json.dump(config, fh, indent=4)
    except IOError as e:
        print('Failed to write config.json')
        sys.exit(1)


def add_auth_params(auth_url, access=None, permissions=None):
    if access is None and permissions is None:
        return auth_url
    parts = urlsplit(auth_url)
    query = parse_qsl(parts.query, True)
    if access is not None:
        query.append(('Access', access))
    if permissions is not None:
        query.append(('Permissions', permissions))
    return urlunsplit((
        parts.scheme,
        parts.netloc,
        parts.path,
        urlencode(query, True),
        parts.fragment))

