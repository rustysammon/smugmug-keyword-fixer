#!/usr/bin/env python3

'''
This console application allows the user to modify his Smugmug keywords for all images
in his galleries.  It will find all photos with a particular keyword (ex: "kiteboarding")
and change each of them to use a new keyword (ex: "kitesurfing") instead.

It's also possible to delete a keyword, or to replace a single keyword with multiple keywords.


'''

from rauth import OAuth1Session
import sys
import common
from common import API_ORIGIN
import textwrap, unicodedata, json

session = None  # our OAuth session with Smugmug

def main():
    # If this is the first time running the program, you'll need to authorize the app
    global session
    session = common.get_session()
    if not session:
        print("Looks like you haven't logged in before. So you'll need to go through the OAuth process")
        oauth_via_console()

        # Generate the OAuthSession
        session = common.get_session()

    # At this point, we should have a valid OAuth session
    # Get the user info to confirm that everything is working properly
    assert session
    response = session.get(API_ORIGIN + '/api/v2!authuser',
                           headers={'Accept': 'application/json'})
    if response.status_code != 200:
        print('GET failed with status code', response.status_code)
        print(response.request.method, response.request.url, '\n', response.request.headers, response.request.body)
        print(response.text)
    else:
        json = response.json()
        user = json['Response']['User']
        name = user['Name']
        nickname = user['NickName']
        user_id = user['Uri']
        print("Welcome {name}, you're all logged in".format(name=name))

    # Let the user change a keyword
    while True:
        keyword = input('What keyword would you like to change? (Enter for list, space to stop) >>')
        if not keyword:
            # Get the list of all keywords
            top_keywords = get_top_keywords(nickname)
            print('You have %d top keywords' % (len(top_keywords)))
            print(textwrap.fill(top_keywords.__str__(), 100))
            print('You can also review them online at:\n', user['WebUri'] + '/keyword')
        elif keyword == unicodedata.lookup('SPACE'):
            print('Done modifying keywords')
            break;
        else:
            # Search for images with that keyword
            images = search_images(user_id, keyword)
            if len(images) == 0:
                print('Could not find keyword "%s" in any images' % (keyword))
                continue;
            print('Keyword "%s" is used in these %d images' % (keyword, len(images)))

            # Prompt the user for the replacement keywords, if any
            for image in images:
                print(image['WebUri'])
            replace_str = input('Replacement keywords (separated by semicolons, "DELETE" to delete) >>')
            if not replace_str:
                print('Stopping replacement.  No changes were made for keyword', keyword)
                break;
            elif replace_str.upper() == 'DELETE':
                # delete the keyword from all the images
                for image in images:
                    try:
                        keywordList = image['KeywordArray']
                        keywordList.remove(keyword)
                        image['KeywordArray'] = keywordList
                        update_keywords(image)
                    except Exception:
                        print('Failed to update image', image['WebUri'],
                              "\nThis sometimes happens if you recently changed the keyword and Smugmug hasn't",
                              "updated their keyword index yet.")
            else:
                # replace the keyword with the replaceList keywords
                replaceList = replace_str.split(';')
                replaceList = [text.strip() for text in replaceList];
                for image in images:
                    try:
                        keywordList = image['KeywordArray']
                        keywordList.remove(keyword)
                        keywordList.extend(replaceList)
                        image['KeywordArray'] = keywordList
                        update_keywords(image)
                    except Exception:
                        print('Failed to update image', image['WebUri'],
                              "\nThis sometimes happens if you recently changed the keyword and Smugmug hasn't",
                              "updated their keyword index yet.")
        print('\n')


def oauth_via_console():
    '''
    Guide the user through the process of Smugmug OAuth for a console app.
    This code is copied from the Smugmug API sample
    '''
    service = common.get_service()

    # First, we need a request token and secret, which SmugMug will give us.
    # We are specifying "oob" (out-of-band) as the callback because we don't
    # have a website for SmugMug to call back to.
    rt, rts = service.get_request_token(params={'oauth_callback': 'oob'})

    # Second, we need to give the user the web URL where they can authorize our
    # application.
    auth_url = common.add_auth_params(service.get_authorize_url(rt), access='Full', permissions='Modify')
    print('In a web browser, go to:\n', auth_url)

    # Once the user has authorized our application, they will be given a
    # six-digit verifier code. Our third step is to ask the user to enter that
    # code:
    sys.stdout.write('Enter the six-digit code: ')
    sys.stdout.flush()
    verifier = sys.stdin.readline().strip()

    # Finally, we can use the verifier code, along with the request token and
    # secret, to sign a request for an access token.
    at, ats = service.get_access_token(rt, rts, params={'oauth_verifier': verifier})

    # The access token we have received is valid forever, unless the user
    # revokes it.  Save it to the config file
    common.save_access_token(at, ats)

def get_top_keywords(user):
    '''
    Get the most common keywords for this user.  Max 1000 keywords as of Oct 2016
    :param user: The user object provided from smugmgug.  It includes the URI of the keywords endpoint
    :return: list of the top keywords for this user.
    '''
    response = session.get(API_ORIGIN + user['TopKeywords']['Uri'],
                           headers={'Accept': 'application/json'})
    return response.json()['Response']['UserTopKeywords']['TopKeywords']


def search_images(scope, keywords):
    '''
    Search for images with certain keywords

    :param scope: The URI of an Album, Folder, Node, or User to search within
    :param keyword: Single keyword or array of keywords to search for
    :return list of images.  Each image is a dictionary.  List will be empty if not images were found
    '''
    response = session.get(API_ORIGIN + '/api/v2/image!search',
                           params={"Scope":scope, "Keywords":keywords},
                           headers={'Accept': 'application/json'})
    try:
        images = response.json()['Response']['Image']
        return images
    except KeyError:
        return []


def update_keywords(image):
    '''
    Update the keywords for an image.
    Note that the image object includes both a *Keywords* field (text) and a *KeywordArray* field (a list).
    This function only pays attention to the *KeywordArray* field.  The *Keywords* field is ignored.
    :param image: image object, with the desired KeywordArray
    '''
    print('Updating keywords for', image['WebUri'])
    print('New keywords are', image['KeywordArray'])
    response = session.patch(API_ORIGIN + image['Uri'],
                             data=json.dumps({'KeywordArray':image['KeywordArray']}),
                             headers={'Content-Type':'application/json',
                                      'Accept': 'application/json',})

if __name__ == '__main__':
    main()

